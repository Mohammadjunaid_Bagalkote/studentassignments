﻿using InvoiceProjectinMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceProjectinMVC.Controllers
{
    public class AddController : Controller
    {
        invoicelistanddetailsEntities db = new invoicelistanddetailsEntities();
        // GET: Add
        public ActionResult Index()
        {
            var data = db.Invoice_List.ToList();
            return View(data);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Invoice_List l)
        {
            if (ModelState.IsValid == true)
            {
                db.Invoice_List.Add(l);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["InsertMsg"] = "<script>alert('Inserted')</script>";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["InsertMsg"] = "<script>alert('Not Inserted')</script>";
                }
            }
            return View();
        }
        public ActionResult Edit(int id)
        {

            var row = db.Invoice_List.Where(model => model.Invoice_id == id).FirstOrDefault();
            return View(row);
        }
        [HttpPost]
        public ActionResult Edit(Invoice_List i)
        {
            if (ModelState.IsValid == true)
            {
                db.Entry(i).State = System.Data.Entity.EntityState.Modified;
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["UpdateMsg"] = "<script>alert('Updated')</script>";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["UpdateMsg"] = "<script>alert('Not Updated')</script>";
                }
            }
            return View();
        }
        public ActionResult Delete(int id)
        {
            var row = db.Invoice_List.Find(id);
            return View(row);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice_List data = db.Invoice_List.Find(id);
            db.Invoice_List.Remove(data);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
        
        public ActionResult CreateForm()
        {
            var list = new List<string>() { "Mahesh", "Ramesh", "Suresh" };
            ViewBag.list = list;
            var list1 = new List<string>() { "Paid", "Sent", "Draft" };
            ViewBag.list1 = list1;
            var list2 = new List<string>() { "Product1", "Product2", "Product3" };
            ViewBag.list2 = list2;
            return View();

        }
        [HttpPost]
        public ActionResult CreateForm(ViewModel v)
        {
            var list = new List<string>() { "Mahesh", "Ramesh", "Suresh" };
            ViewBag.list = list;
            var list1 = new List<string>() { "Paid", "Sent", "Draft" };
            ViewBag.list1 = list1;
            var list2 = new List<string>() { "Product1", "Product2", "Product3" };
            ViewBag.list2 = list2;
           

            if (ModelState.IsValid == true)
            {
                db.Invoice_List.Add(v.model1);

                db.Invoice_Details.Add(v.model2);
                
              //  db.Invoice_Details.Add(v.model2);
                int a = db.SaveChanges();
                if (a > 0)
                {
                    TempData["InsertMsg"] = "<script>alert('Inserted')</script>";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["InsertMsg"] = "<script>alert('Not Inserted')</script>";
                }
            }
            return View();
        }
    }
}