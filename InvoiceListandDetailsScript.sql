USE [invoicelistanddetails]
GO
/****** Object:  Table [dbo].[Invoice_Details]    Script Date: 12/30/2021 2:45:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Details](
	[Product_id] [int] NOT NULL,
	[Product] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Price] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[Tax] [int] NOT NULL,
	[Total] [int] NOT NULL,
	[SubTotal] [int] NOT NULL,
	[TotalTax] [int] NOT NULL,
	[GrandTotal] [int] NOT NULL,
	[Invoice_id] [int] NOT NULL,
 CONSTRAINT [PK_Invoice_Details] PRIMARY KEY CLUSTERED 
(
	[Product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoice_List]    Script Date: 12/30/2021 2:45:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_List](
	[Invoice_id] [int] IDENTITY(1,1) NOT NULL,
	[Invoice_No] [nvarchar](50) NOT NULL,
	[Bill_To] [nvarchar](50) NOT NULL,
	[Invoice_Date] [date] NOT NULL,
	[Due_Date] [date] NOT NULL,
	[Amount] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Invoice_List] PRIMARY KEY CLUSTERED 
(
	[Invoice_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Invoice_Details] ([Product_id], [Product], [Description], [Price], [Qty], [Tax], [Total], [SubTotal], [TotalTax], [GrandTotal], [Invoice_id]) VALUES (2, N'ws', N'200gb', 100, 1, 10, 100, 0, 0, 0, 3)
GO
SET IDENTITY_INSERT [dbo].[Invoice_List] ON 

INSERT [dbo].[Invoice_List] ([Invoice_id], [Invoice_No], [Bill_To], [Invoice_Date], [Due_Date], [Amount], [Status]) VALUES (2, N'Inv-001', N'suresh', CAST(N'2021-12-26' AS Date), CAST(N'2021-12-29' AS Date), 300, N'Sent')
INSERT [dbo].[Invoice_List] ([Invoice_id], [Invoice_No], [Bill_To], [Invoice_Date], [Due_Date], [Amount], [Status]) VALUES (3, N'Inv-006', N'Ramesh', CAST(N'2021-12-12' AS Date), CAST(N'2021-12-29' AS Date), 300, N'Paid')
SET IDENTITY_INSERT [dbo].[Invoice_List] OFF
GO
ALTER TABLE [dbo].[Invoice_Details]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Details_Invoice_List] FOREIGN KEY([Invoice_id])
REFERENCES [dbo].[Invoice_List] ([Invoice_id])
GO
ALTER TABLE [dbo].[Invoice_Details] CHECK CONSTRAINT [FK_Invoice_Details_Invoice_List]
GO
